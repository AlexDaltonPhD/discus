use serde::{Serialize, Deserialize};
use std::{
    io::{Write, Read},
    collections::HashSet,
};
use rug::Integer;
use crate::cryptosystems::lfhe::{LFHE, LFHEParameters};
use crate::cryptosystems::bfv::{BFV, BFVParameters, BFVCipherText, BFVPublicKey, BFVEvaluationKey, BFVSecretKey};
use crate::cryptosystems::bfv;

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub enum Operation {
    Add, Sub, Mul, Div,
    LessThan, Equal,
    Enc, Dec
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct GlobalInstruction {
    operation: Operation,
    operands: Vec<Operand>,
    results: Vec<OperandID>,
    local: bool,
}

impl GlobalInstruction {
    pub fn operation(&self) -> Operation { self.operation }
    pub fn operands(&self) -> &Vec<Operand> { &self.operands }
    pub fn results(&self) -> &Vec<OperandID> { &self.results }
    pub fn local(&self) -> bool { self.local }

    pub fn get_wait_op(&self) -> Option<Operand> {
        if !self.results.is_empty() {
            Some(Operand::new(self.results[0], Data::Wait, self.local))
        } else {
            None
        }
    }
}

// Translation of Operations from OperandID to using data indexes
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct LocalInstruction {
    operation: Operation,
    operands: Vec<usize>,
    results: Vec<usize>
}

impl LocalInstruction {
    pub fn new(operation: Operation, operands: Vec<usize>, results: Vec<usize>) -> LocalInstruction {
        LocalInstruction { operation, operands, results }
    }
    pub fn operation(&self) -> Operation { self.operation }
    pub fn operands(&self) -> &Vec<usize> { &self.operands }
    pub fn results(&self) -> &Vec<usize> { &self.results }
}

type OperandID = u64;
pub type FragmentID = u64;

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum Data { None, Wait, Int(u64), CipherText(BFVCipherText), PublicKey(BFVParameters, (BFVPublicKey, BFVEvaluationKey)), SecretKey(BFVSecretKey) }

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Operand {
    id: OperandID,
    data: Data,
    local: bool,
}

impl Operand {
    pub fn new(id: OperandID, data: Data, local: bool) -> Operand { Operand { id, data, local } }
    pub fn id(&self) -> OperandID { self.id }
    pub fn data(&self) -> &Data { &self.data }
    pub fn set_data(&mut self, data: Data) { self.data = data }
    pub fn set_remote(&mut self) { self.local = false; }
    pub fn local(&self) -> bool { self.local }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Fragment {
    id: FragmentID,
    instructions: Vec<LocalInstruction>,
    operands: Vec<Operand>,
    done: bool,
    local: bool,
}

impl Fragment {
    pub fn new(id: FragmentID) -> Self {
        Fragment {
            id,
            instructions: Vec::new(),
            operands: Vec::new(),
            done: false,
            local: false,
        }
    }

    pub fn id(&self) -> FragmentID { self.id }
    pub fn instructions(&self) -> &Vec<LocalInstruction> { &self.instructions }
    pub fn operands(&self) -> &Vec<Operand> { &self.operands }
    pub fn operands_mut(&mut self) -> &mut Vec<Operand> { &mut self.operands }
    pub fn done(&self) -> bool { self.done }
    pub fn local(&self) -> bool { self.local }

    fn set_local(&mut self) { self.local = true; }

    fn get_op_index(&self, id: OperandID) -> Option<usize> {
        for i in 0..self.operands.len() {
            if self.operands[i].id == id { return Some(i); }
        }

        None
    }

    // Translates a GlobalInstruction into a LocalInstruction, constructs any
    // required placeholder memory and appends the instruction
    pub fn append_instruction(&mut self, i: &GlobalInstruction) {
        let local_i = match i.operation() {
            Operation::Add |
                Operation::Sub |
                Operation::Mul |
                Operation::Div |
                Operation::LessThan |
                Operation::Equal => {
                    let b_index = match self.get_op_index(i.operands()[0].id()) {
                        Some(i) => i,
                        None => {
                            self.operands.push(i.operands()[0].clone());
                            self.operands.len() - 1
                        }
                    };
                    let c_index = match self.get_op_index(i.operands()[1].id()) {
                        Some(i) => i,
                        None => {
                            self.operands.push(i.operands()[1].clone());
                            self.operands.len() - 1
                        }
                    };
                    let local = i.operands()[0].local || i.operands()[1].local;
                    self.local = local || self.local;
                    self.operands.push(Operand { id: i.results()[0], data: Data::Wait, local});
                    let a_index = self.operands.len() - 1;
                    LocalInstruction {
                        operation: i.operation(),
                        operands: vec![b_index, c_index],
                        results: vec![a_index],
                    }
                },
            Operation::Enc | Operation::Dec => {
                let b_index = match self.get_op_index(i.operands()[0].id()) {
                    Some(i) => i,
                    None => {
                        self.operands.push(i.operands()[0].clone());
                        self.operands.len() - 1
                    }
                };
                let c_index = match self.get_op_index(i.operands()[1].id()) {
                    Some(i) => i,
                    None => {
                        self.operands.push(i.operands()[1].clone());
                        self.operands.len() - 1
                    }
                };
                self.local = true;
                self.operands.push(Operand { id: i.results()[0], data: Data::Wait, local: i.operands()[0].local});
                let a_index = self.operands.len() - 1;
                LocalInstruction {
                    operation: i.operation(),
                    operands: vec![b_index, c_index],
                    results: vec![a_index],
                }
            }
        };

        self.instructions.push(local_i);
    }

    pub fn update(&mut self, index: usize, result: Data) -> Result<(), &'static str> {
        match self.operands[index].data {
            Data::Wait => { self.operands[index].data = result; },
            _ => { return Result::Err("Atempting to update a non-waiting operand"); }
        }

        Result::Ok(())
    }

    pub fn finished(&mut self) {
        self.done = true;
    }

    pub fn ready(&self) -> bool {
        // ready if none of the operands need to be updated by external results
        let mut map: HashSet<usize> = HashSet::new();
        for i in 0..self.operands.len() {
            match self.operands[i].data {
                Data::Wait => { map.insert(i); },
                _ => {},
            }
        }

        for i in &self.instructions {
            for r in &i.results {
                map.remove(r);
            }
        }

        map.is_empty()
    }
}

pub type Program = Vec<Fragment>;

pub fn write_program(program: Vec<GlobalInstruction>, file_name: String) -> Result<(), &'static str> {
    let mut file = std::fs::File::create(file_name).unwrap();
    let json = serde_json::to_string(&program).unwrap();
    file.write_all(json.as_bytes()).unwrap();

    Result::Ok(())
}

pub fn read_program(file_name: String) -> Result<Vec<GlobalInstruction>, &'static str> {
    let mut file = std::fs::File::open(file_name).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    let p: Vec<GlobalInstruction> = serde_json::from_str(&contents).unwrap();

    Result::Ok(p)
}

pub struct State {
    current_fragment_id: FragmentID,
    current_operand_id: OperandID, 
}

impl State {
    pub fn new() -> Self {
        State {
            current_fragment_id: 0,
            current_operand_id: 0,
        }
    }

    fn get_fragment_id(&mut self) -> FragmentID {
        self.current_fragment_id += 1;
        self.current_fragment_id - 1
    }

    fn get_operand_id(&mut self) -> OperandID {
        self.current_operand_id += 1;
        self.current_operand_id - 1
    }

    pub fn gen_fragment(&mut self, instructions: &Vec<GlobalInstruction>) -> Fragment {
        let mut fragment = Fragment::new(self.get_fragment_id());
        for i in instructions {
            fragment.append_instruction(i);
            if i.local() { fragment.set_local(); }
        }

        fragment
    }

    pub fn gen_operand(&mut self, data: Data, local: bool) -> Operand {
        Operand {
            id: self.get_operand_id(),
            data: data,
            local: local,
        }
    }

    pub fn gen_instruction(&mut self, operation: Operation, operands: Vec<Operand>) -> GlobalInstruction {
        match operation {
            Operation::Add | Operation::Sub | Operation::Mul | Operation::Div |
            Operation::LessThan | Operation::Equal => {
                let local = operands[0].local || operands[1].local;
                GlobalInstruction {
                    operation: operation,
                    operands: operands,
                    results: vec![self.get_operand_id()],
                    local,
                }
            },
            Operation::Enc | Operation::Dec => {
                GlobalInstruction {
                    operation: operation,
                    operands: operands,
                    results: vec![self.get_operand_id()],
                    local: true
                }
            }
        }
    }

    // precission - maximum plaintext number of bits
    // levels - maximum number of multiplication operations
    pub fn gen_cryptosystem(&mut self, precission: u64, levels: u32) -> BFV {
        let mut parameters = BFVParameters::new(Integer::from(1 << precission), bfv::K, bfv::LOG_N, bfv::B);
        parameters.set_level(levels);

        BFV::new(&parameters)
    }
}
