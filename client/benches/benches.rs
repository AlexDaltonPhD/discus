#[macro_use]
extern crate bencher;

use bencher::Bencher;
use discus_backend::{interpret, State, Program, Operation, LFHE, Data};

fn fibonacci_20(bench: &mut Bencher){
    let demo = generate_fibonacci_20_demo();
    bench.iter(|| {
        interpret(&mut demo.clone()).unwrap();
    })
}

fn enc_fibonacci_20(bench: &mut Bencher) {
    let demo = generate_enc_fibonacci_20_demo();
    bench.iter(|| {
        interpret(&mut demo.clone()).unwrap();
    })
}

benchmark_group!(benches, fibonacci_20, enc_fibonacci_20);
benchmark_main!(benches);

pub fn generate_fibonacci_20_demo() -> Program {
    let mut state = State::new();
    let mut instructions = Vec::new();
    let op_0 = state.gen_operand(Data::Int(1));
    let op_1 = state.gen_operand(Data::Int(1));
    let op_2 = state.gen_operand(Data::Int(1));
    let instruction_0 = state.gen_instruction(Operation::Add, vec![op_0.clone(), op_1.clone()]);
    let instruction_1 = state.gen_instruction(Operation::Add, vec![instruction_0.get_wait_op().unwrap(), op_2.clone()]);
    instructions.push(instruction_0);
    instructions.push(instruction_1);

    for i in 2..1000 {
        let instruction = state.gen_instruction(Operation::Add, vec![instructions[i-1].get_wait_op().unwrap(), instructions[i-2].get_wait_op().unwrap()]);
        instructions.push(instruction);
    }

    vec![state.gen_fragment(&instructions)]
}

pub fn generate_enc_fibonacci_20_demo() -> Program {
    let mut state = State::new();
    let mut bfv = state.gen_cryptosystem(1024, 10);
    let keys = bfv.keygen().unwrap();
    let m = bfv.plaintext_from(1);
    let mut instructions = Vec::new();
    let op_0 = state.gen_operand(Data::CipherText(bfv.enc(&m.clone(), &keys.1).unwrap()));
    let op_1 = state.gen_operand(Data::CipherText(bfv.enc(&m.clone(), &keys.1).unwrap()));
    let op_2 = state.gen_operand(Data::CipherText(bfv.enc(&m.clone(), &keys.1).unwrap()));
    let instruction_0 = state.gen_instruction(Operation::Add, vec![op_0.clone(), op_1.clone()]);
    let instruction_1 = state.gen_instruction(Operation::Add, vec![instruction_0.get_wait_op().unwrap(), op_2.clone()]);
    instructions.push(instruction_0);
    instructions.push(instruction_1);

    for i in 2..14 {
        let instruction = state.gen_instruction(Operation::Add, vec![instructions[i-1].get_wait_op().unwrap(), instructions[i-2].get_wait_op().unwrap()]);
        instructions.push(instruction);
    }

    vec![state.gen_fragment(&instructions)]
}
