use discus_backend::{State, Data, Operation, GlobalInstruction, LFHE, BFVCipherText};

pub fn generate_saturation_demo(operations: usize) -> Vec<GlobalInstruction> {
    let mut state = State::new();
    let op_0 = state.gen_operand(Data::Int(1), false);
    let op_1 = state.gen_operand(Data::Int(1), false);
    let instruction = state.gen_instruction(Operation::Add, vec![op_0.clone(), op_1.clone()]);
    vec![instruction; operations]
}

pub fn generate_he_saturation_demo(operations: usize) -> Vec<GlobalInstruction> {
    let mut state = State::new();
    let mut crypto = state.gen_cryptosystem(8, 1);
    let keys = crypto.keygen().unwrap();
    let mut a_m = crypto.plaintext();
    a_m.pack(0, 2);
    let mut b_m = crypto.plaintext();
    b_m.pack(0, 6);

    let op_0 = state.gen_operand(Data::CipherText(BFVCipherText::enc(&a_m, crypto.parameters(), &keys.1).unwrap()), false);
    let op_1 = state.gen_operand(Data::CipherText(BFVCipherText::enc(&b_m, crypto.parameters(), &keys.1).unwrap()), false);
    let instruction = state.gen_instruction(Operation::Mul, vec![op_0.clone(), op_1.clone()]);

    vec![instruction; operations]
}

pub fn generate_he_3d_path_length_demo(p1: (u8, u8, u8), p2: (u8, u8, u8)) -> Vec<GlobalInstruction> {
    let mut state = State::new();
    let mut crypto = state.gen_cryptosystem(8, 2);
    let keys = crypto.keygen().unwrap();

    let mut p1_x = crypto.plaintext();
    p1_x.pack(0, p1.0.into());
    let mut p1_y = crypto.plaintext();
    p1_y.pack(0, p1.1.into());
    let mut p1_z = crypto.plaintext();
    p1_z.pack(0, p1.2.into());

    let mut p2_x = crypto.plaintext();
    p2_x.pack(0, p2.0.into());
    let mut p2_y = crypto.plaintext();
    p2_y.pack(0, p2.1.into());
    let mut p2_z = crypto.plaintext();
    p2_z.pack(0, p2.2.into());

    let p1_x_op = state.gen_operand(Data::CipherText(BFVCipherText::enc(&p1_x, crypto.parameters(), &keys.1).unwrap()), false);
    let p1_y_op = state.gen_operand(Data::CipherText(BFVCipherText::enc(&p1_y, crypto.parameters(), &keys.1).unwrap()), false);
    let p1_z_op = state.gen_operand(Data::CipherText(BFVCipherText::enc(&p1_z, crypto.parameters(), &keys.1).unwrap()), false);
    let p2_x_op = state.gen_operand(Data::CipherText(BFVCipherText::enc(&p2_x, crypto.parameters(), &keys.1).unwrap()), false);
    let p2_y_op = state.gen_operand(Data::CipherText(BFVCipherText::enc(&p2_y, crypto.parameters(), &keys.1).unwrap()), false);
    let p2_z_op = state.gen_operand(Data::CipherText(BFVCipherText::enc(&p2_z, crypto.parameters(), &keys.1).unwrap()), false);

    let mut i = Vec::new();
    i.push(state.gen_instruction(Operation::Sub, vec![p2_x_op, p1_x_op]));
    i.push(state.gen_instruction(Operation::Sub, vec![p2_y_op, p1_y_op]));
    i.push(state.gen_instruction(Operation::Sub, vec![p2_z_op, p1_z_op]));
    i.push(state.gen_instruction(Operation::Mul, vec![i[0].get_wait_op().unwrap(), i[0].get_wait_op().unwrap()]));
    i.push(state.gen_instruction(Operation::Mul, vec![i[1].get_wait_op().unwrap(), i[1].get_wait_op().unwrap()]));
    i.push(state.gen_instruction(Operation::Mul, vec![i[2].get_wait_op().unwrap(), i[2].get_wait_op().unwrap()]));
    i.push(state.gen_instruction(Operation::Add, vec![i[3].get_wait_op().unwrap(), i[4].get_wait_op().unwrap()]));
    i.push(state.gen_instruction(Operation::Add, vec![i[6].get_wait_op().unwrap(), i[5].get_wait_op().unwrap()]));
    //TODO: Need Division to perform final square root

    i
}

pub fn generate_he_linear_regression(x: Vec<u8>, y: Vec<u8>) -> Vec<GlobalInstruction> {
    let mut state = State::new();
    let mut crypto = state.gen_cryptosystem(8, 2);
    let keys = crypto.keygen().unwrap();
    let mut enc_x = Vec::new();
    let mut enc_y = Vec::new();
    let mut inst = Vec::new();

    for x_i in x {
        let mut x_m = crypto.plaintext();
        x_m.pack(0, x_i.into());
        enc_x.push(state.gen_operand(Data::CipherText(BFVCipherText::enc(&x_m, crypto.parameters(), &keys.1).unwrap()), false));
    }
    for y_i in y {
        let mut y_m = crypto.plaintext();
        y_m.pack(0, y_i.into());
        enc_y.push(state.gen_operand(Data::CipherText(BFVCipherText::enc(&y_m, crypto.parameters(), &keys.1).unwrap()), false));
    }

    // average over x
    inst.push(state.gen_instruction(Operation::Add, vec![enc_x[0].clone(), enc_x[1].clone()]));
    for i in 2..enc_x.len() {
        inst.push(state.gen_instruction(Operation::Add, vec![inst[0].get_wait_op().unwrap(), enc_x[i].clone()]));
    }
    //TODO: sum(x) / enc_x.len()
    //TODO: average over y
    //TODO: SS_xy = sum(x*y) - n*mean(y)*mean(x)
    //TODO: SS_xx = sum(x*x) - n*mean(x)*mean(x)
    //TODO: b_1 = SS_xy / SS_xx
    //TODO: b_0 = m_y - b_1 * m_x

    inst
}
