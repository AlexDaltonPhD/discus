use async_std::{prelude::*, fs};
use std::sync::{Mutex, Arc};
use crate::protocol::{WorkRequest, WorkResponse};
use crate::behaviour::DSCSBehaviour;
use crate::work_future::{WorkSharedState, WorkResponseFuture, FragmentSharedState, FragmentReadyFuture};
use libp2p::Swarm;
use discus_backend::{
    read_program,
    execute_fragment,
    State, Fragment, Program, Operand, FragmentID, GlobalInstruction
};
use futures::stream::futures_unordered::FuturesUnordered;
use futures::FutureExt;
use crate::benchmarking;
use time::Instant;
use rand;

pub async fn process_work_request(request: &mut WorkRequest) -> WorkResponse {
    if request.fragment.ready() {
        info!("Executing fragment {:?}", request.fragment);
        if let Err(e) = execute_fragment(&mut request.fragment) {
            error!("Unable to execute fragment: {:?}", e);
        } else {
            info!("Executed fragment {:?}", request.fragment);
        }
    } else {
        error!("Next fragment not ready {:?}", request.fragment);
    }

    WorkResponse {
        results: request.fragment.operands().clone(),
        id: request.fragment.id()
    }
}

pub async fn handle_list_peers(swarm: &mut Swarm<DSCSBehaviour>) {
    let peers = swarm.behaviour().peers.lock().unwrap();
    for peer in peers.iter() {
        println!("{:?}", peer);
    }
}

pub async fn handle_list_programs() {
    let mut paths = fs::read_dir("./fragments").await.unwrap();

    while let Some(Ok(path)) = paths.next().await {
        println!("{}", path.file_name().to_string_lossy());
    }
}

fn request(queue: Arc<Mutex<Vec<(WorkRequest, Arc<Mutex<WorkSharedState>>)>>>, request: WorkRequest) -> WorkResponseFuture {
    let future = WorkResponseFuture::new();
    let mut queue_lock = queue.lock().unwrap();
    queue_lock.push((request, future.shared_state.clone()));
    future
}

//TODO: dynamic program chunking based on environmental conditions
fn chunk(program: Vec<GlobalInstruction>) -> Program {
    let mut ret = Vec::new();
    let mut state = State::new();
    for f in program {
        ret.push(state.gen_fragment(&vec![f]));
    }
    ret
}

enum ChunkEvent { ChunkReady(Fragment), ChunkDone(FragmentID, Vec<Operand>), LocalChunkDone(Vec<Operand>) }

async fn run_program(queue: Arc<Mutex<Vec<(WorkRequest, Arc<Mutex<WorkSharedState>>)>>>, chunked: Program) {
    let shared_states = Arc::new(Mutex::new(Vec::new()));
    let mut ready_chunks: FuturesUnordered<FragmentReadyFuture> = FuturesUnordered::new();
    let mut executing_chunks: FuturesUnordered<WorkResponseFuture> = FuturesUnordered::new();
    {
        let mut shared_states_locked = shared_states.lock().unwrap();
        for f in chunked {
            let shared_state = Arc::new(Mutex::new(FragmentSharedState::new(f)));
            ready_chunks.push(FragmentReadyFuture::new(shared_state.clone()));
            shared_states_locked.push(shared_state.clone());
        }
    }

    let mut all_ready = false;

    loop {
        let event = futures::select! {
            chunk_ready = ready_chunks.next().fuse() => {
                match chunk_ready {
                    Some(state) => {
                        debug!("Chunk registered as ready");
                        let mut locked = state.lock().unwrap();
                        if locked.fragment.local() {
                            execute_fragment(&mut locked.fragment).unwrap();
                            debug!("Local chunk execution");
                            Some(ChunkEvent::LocalChunkDone(locked.fragment.operands().clone()))
                        } else {
                            debug!("Queuing remote chunk execution");
                            Some(ChunkEvent::ChunkReady(locked.fragment.clone()))
                        }
                    },
                    None => { all_ready = true; None },
                }
            },
            chunk_done = executing_chunks.next().fuse() => {
                match chunk_done {
                    Some(response) => {
                        debug!("Chunk registered as done");
                        Some(ChunkEvent::ChunkDone(response.id, response.results.clone()))
                    },
                    None => { if all_ready { return } None },
                }
            }
        };

        if let Some(e) = event {
            match e {
                ChunkEvent::ChunkReady(f) => {
                    executing_chunks.push(request(queue.clone(), WorkRequest { fragment: f }));
                },
                ChunkEvent::ChunkDone(id, o) => {
                    let shared_states_local = shared_states.clone();
                    let shared_states_lock = shared_states_local.lock().unwrap();
                    for s in 0..shared_states_lock.len() {
                        let mut state_lock = shared_states_lock[s].lock().unwrap();
                        state_lock.update(&o);
                    }
                }
                ChunkEvent::LocalChunkDone(o) => {
                    let shared_states_local = shared_states.clone();
                    let shared_states_lock = shared_states_local.lock().unwrap();
                    for s in 0..shared_states_lock.len() {
                        let mut state_lock = shared_states_lock[s].lock().unwrap();
                        state_lock.update(&o);
                    }
                },
            }
        }
    }
}

pub async fn handle_run_program(queue: Arc<Mutex<Vec<(WorkRequest, Arc<Mutex<WorkSharedState>>)>>>, program_name: String) {
    if let Result::Ok(program) = read_program(String::from("./fragments/") + &program_name) {
        info!("Starting program {}", program_name);
        let chunked = chunk(program);
        run_program(queue, chunked).await;
    } else {
        error!("Unable to find fragments file {}", program_name);
    }
}

pub async fn benchmark(queue: Arc<Mutex<Vec<(WorkRequest, Arc<Mutex<WorkSharedState>>)>>>) {
    println!("========== benchmarking ==========");
    let repeats = 1;
    //TODO: change these metrics to operations per second

    {
        println!("running saturation benchmark...");
        let steps = 1000;
        let saturation_demo = chunk(benchmarking::generate_saturation_demo(steps));
        let mut i_per_s: Vec<f64> = Vec::new();
        let mut time: Vec<f64> = Vec::new();
        for _ in 0..repeats {
            let start = Instant::now();
            run_program(queue.clone(), saturation_demo.clone()).await;
            let end = start.elapsed();
            i_per_s.push((steps as f64 / (end.whole_nanoseconds() as f64)) * 1_000_000_000 as f64);
            time.push(end.whole_nanoseconds() as f64 / 1_000_000_000 as f64);
        }
        println!("Avg: {:.2} instructions per second from {} runs, avg run time {:.3}s",
                 i_per_s.iter().sum::<f64>() / (i_per_s.len() as f64),
                 time.iter().sum::<f64>() / (time.len() as f64),
                 time.len());
    }

    {
        println!("running homomorphic saturation benchmark...");
        let steps = 100;
        let saturation_demo = chunk(benchmarking::generate_he_saturation_demo(steps));
        let mut i_per_s: Vec<f64> = Vec::new();
        let mut time: Vec<f64> = Vec::new();
        for _ in 0..repeats {
            let start = Instant::now();
            run_program(queue.clone(), saturation_demo.clone()).await;
            let end = start.elapsed();
            i_per_s.push((steps as f64 / (end.whole_nanoseconds() as f64)) * 1_000_000_000 as f64);
            time.push(end.whole_nanoseconds() as f64 / 1_000_000_000 as f64);
        }
        println!("Avg: {:.2} instructions per second from {} runs, avg run time {:.3}s",
                 i_per_s.iter().sum::<f64>() / (i_per_s.len() as f64),
                 time.iter().sum::<f64>() / (time.len() as f64),
                 time.len());
    }

    {
        println!("running homomorphic 3d path length benchmark...");
        let mut time: Vec<f64> = Vec::new();
        for _ in 0..repeats {
            let p1: (u8, u8, u8) = (rand::random::<u8>(), rand::random::<u8>(), rand::random::<u8>());
            let p2: (u8, u8, u8) = (rand::random::<u8>(), rand::random::<u8>(), rand::random::<u8>());
            let demo = chunk(benchmarking::generate_he_3d_path_length_demo(p1, p2));
            let start = Instant::now();
            run_program(queue.clone(), demo).await;
            let end = start.elapsed();
            time.push(end.whole_nanoseconds() as f64 / 1_000_000_000 as f64);
        }
        println!("Avg: {:.3}s from {} runs",
                 time.iter().sum::<f64>() / (time.len() as f64),
                 time.len());
    }
}
