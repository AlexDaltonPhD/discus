#[macro_use]
extern crate log;
extern crate time;
extern crate clap;

use clap::{Arg, App, SubCommand};
use libp2p::Multiaddr;
use pretty_env_logger;
use async_std::task;

mod processing;
mod protocol;
mod behaviour;
mod network;
mod work_future;
mod benchmarking;

fn main() {
    pretty_env_logger::init();

    let matches = App::new("DiSCoS")
        .version("0.1.0")
        .author("Alex Dalton")
        .about("Distributed Secure Computation System")
        .subcommand(SubCommand::with_name("fragment")
                    .about("fragment a program")
                    .arg(Arg::with_name("file")
                         .help("program file")
                         .required(true)
                         .index(1)))
        .subcommand(SubCommand::with_name("interpret")
                    .about("interpret fragmented program")
                    .arg(Arg::with_name("file")
                         .help("program file")
                         .required(true)
                         .index(1)))
        .subcommand(SubCommand::with_name("networkd")
                    .about("connect to the network"))
                    .arg(Arg::with_name("remote")
                         .short("r")
                         .long("remote")
                         .takes_value(true)
                         .help("remote address for network demon to connect to")
                         .required(false))
        .get_matches();

	if let Some(_m) = matches.subcommand_matches("fragment") {
        println!("Error: Fragmentation not implemented");
	}

	if let Some(_m) = matches.subcommand_matches("interpret") {
        println!("Error: Interpreter not implemented");
	}

	if let Some(_m) = matches.subcommand_matches("networkd") {
        let remote_addr: Option<Multiaddr> = match matches.value_of("remote") {
            Some(r) => Some(r.parse().unwrap()),
            None => None,
        };
        info!("Starting network daemon");

        let networkd = task::spawn(async {
            match network::connect(remote_addr).await {
                Result::Ok(()) => info!("Network daemon terminated successfully"),
                Result::Err(e) => error!("Error: {:?}", e),
            }
        });

        info!("Started network daemon");
        task::block_on(networkd);
        info!("Stopped network daemon");
    }
}
